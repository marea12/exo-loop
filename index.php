<!--##Exercice 1
Créer une variable et l'initialiser à 0.  
Tant que cette variable n'atteint pas 10, il faut :
- l'afficher
- l'incrementer-->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <?php
for ($i=0; $i<10; $i++) {
    echo 'Ceci est une boucle for en PHP' . "<br>";
}
?>
<!--Créer deux variables. Initialiser la première à 0 et la deuxième avec un nombre compris en 1 et 100.  
Tant que la première variable n'est pas supérieur à 20 :
- multiplier la première variable avec la deuxième
- afficher le résultat
- incrementer la première variable-->
<?php
$premiere=3;
$deuxieme=5;
for ($i=$premiere; $i<20;$i++) {
    echo $i*$deuxieme . "<br>"; 
}
?>
<!--##Exercice 3
Créer deux variables. Initialiser la première à 100 et la deuxième avec un nombre compris en 1 et 100.  
Tant que la première variable n'est pas inférieur ou égale à 10 :
- multiplier la première variable avec la deuxième
- afficher le résultat
- décrémenter la première variable-->
<?php
$premiere1=100;
$deuxieme2=8;
for ($i=$premiere1; $i>=10;$i--) {
    echo $i*$deuxieme2 . "<br>"; 
}
?>


<?php
$i=1;
for ($i=1; $i<10;$i+=$i/2) {
    echo $i . "<br>";
}
?>
<!--##Exercice 5
En allant de 1 à 15 avec un pas de 1, afficher le message **On y arrive presque**.--> 
<?php
$exo="On y arrive presque";
for ($i=1; $i<=15;$i++) {
    echo $i . "<br>";
}
?>
<!--##Exercice 6
En allant de 20 à 0 avec un pas de 1, afficher le message **C'est presque bon**.--> 
<?php
$exo="On y arrive presque";
for ($i=20; $i>=0;$i--) {
    echo $exo . "<br>";
}
?>
<!--##Exercice 7
En allant de 1 à 100 avec un pas de 15, afficher le message **On tient le bon bout**.--> 
<?php
$exo1="On tient le bout";
for ($i=1; $i<=100;$i=$i+15) {
    echo $exo1 . "<br>";
}
?>
<!--##Exercice 8
En allant de 200 à 0 avec un pas de 12, afficher le message **Enfin !!!!**.-->
<?php
$exo8="enfin";
for ($i=200; $i>=0;$i=$i-12) {
    echo $exo8 . "<br>";
}
?>
</body>
</html>